<?php

/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 21/11/2015
 * Time: 15:02
 */
require_once '../index.php';
abstract class MY_Test extends PHPUnit_Framework_TestCase    {


    protected $errors;



    /* public static $arrErr = array();*/
    protected function setUp()    {
        $this->errors = array();
        set_error_handler(array($this, "errorHandler"));
    }
    public function errorHandler($errno, $errstr, $errfile, $errline, $errcontext)    {
        if ( strrpos($errstr , 'Cannot modify header information - headers already sent by') === false ){
            $this->errors[] = compact("errno", "errstr", "errfile", "errline", "errcontext");
        }

    }

    public function assertError($errstr, $errno)    {
        foreach ($this->errors as $error) { if ($error["errstr"] === $errstr && $error["errno"] === $errno) { return; } }
        $this->fail("Error with level " . $errno . " and message '" . $errstr . "' not found in ", var_export($this->errors, TRUE));
    }
    
}