<?php

/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 5/20/2016
 * Time: 4:18 PM
 */
class Cell{
    protected $x;
    protected $y;
    public function __construct($x = null , $y = null){
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @param $name
     * @return int
     */
    public function __get($name)
    {
        if($name == 'x') return $this->x;
        if($name == 'y') return $this->y;

    }

    /**
     * @param $name
     * @param $value int
     */
    public function __set($name, $value)
    {
        if($name == 'x') $this->x = $value;
        if($name == 'y') $this->y = $value;
        
    }
   
}