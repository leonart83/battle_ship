<?php

/**
 * Created by PhpStorm.
 * User: latalla
 * Date: 23/09/2015
 * Time: 11:05
 */
require_once('./MY_Test.php');

class Game_Test extends MY_Test {

    private $grid;
    public function setUp() {
        parent::setUp();
        $this->grid = new Grid();
    }
    public function tearDown() {
        if (count($this->errors)){
            print_r($this->errors);
        }
        $this->assertEmpty($this->errors);
    }
    /** @test */
    public function Test_Ship_place_H()    {
        $boat = new Ship(4);
        $boat->setCoordinates(new Cell(0,0) , new Cell(4,0 ));
        $this->assertTrue($boat->addTo($this->grid));
        $this->assertEquals($boat->getOrientation(), Ship::HORIZONTAL);
        $data = $this->grid->getDataGrid();
        $this->assertEquals($data['A'][0],2);
        $this->assertEquals($data['A'][1],2);
        $this->assertEquals($data['A'][2],2);
        $this->assertEquals($data['A'][3],2);
        $this->assertEquals($data['A'][4],0);
        //echo $this->grid->output();
    }

    /** @test */
    public function Test_Ship_place_V()    {
        $boat = new Ship(4);
        $boat->setCoordinates(new Cell(0,0) , new Cell(0,4));
        $this->assertTrue($boat->addTo($this->grid));
        $this->assertEquals($boat->getOrientation(), Ship::VERTICAL);
        $data = $this->grid->getDataGrid();
        $this->assertEquals($data['A'][0],2);
        $this->assertEquals($data['B'][0],2);
        $this->assertEquals($data['C'][0],2);
        $this->assertEquals($data['D'][0],2);
        $this->assertEquals($data['A'][1],0);
        $this->assertEquals($data['B'][1],0);
        $this->assertEquals($data['C'][1],0);
        $this->assertEquals($data['D'][1],0);
        //echo $this->grid->output();

    }

    /** @test */
    public function Test_Ship_validate_Space()    {
        $temp =  array(0,0,0,0);
        $r = Ship::validate_space($temp);
        $this->assertTrue( $r );
        $temp =  array(0,1,0,0);
        $r = Ship::validate_space($temp);

        $this->assertFalse( $r );
    }

    /** @test */
    public function Test_Is_Test_Env()    {
        $g = new BattleShipGame();
        $this->assertTrue($g->get_is_test_env());

    }

    public function Test_Attack()    {
        $game = new BattleShipGame();
        $cols = $this->grid->getColHeaders();
        $rows = $this->grid->getRowHeaders();
        foreach ($rows as $r ){
            foreach ($cols as $i => $c){
                $coord =  $r.$c;
                $this->assertTrue($game->validate($coord));
                /* $game->attack();*/
            }
        }
    }
    /** @test */
    public function Test_Ship_Factory()    {
        $game = new BattleShipGame();
        $game->debug_console = true;
        $sess_man = $game->getSessionManager();
        $this->assertNotNull($sess_man);
        $this->assertInstanceOf('SessionManager', $sess_man );

        $ship_f = $game->getSHipFactory();
        $this->assertNotNull($ship_f);
        $this->assertInstanceOf('ShipFactory', $ship_f );
        $ship_f->createShips();

        $boats = $ship_f->getShips();

        $this->assertNotNull($boats);
        $this->assertInstanceOf('Ship', $boats[0] );
    }
    /** @test */
    public function Test_Ship_create()    {
        $b = new Ship(4);
        $this->assertNotNull($b);
        $this->assertClassHasAttribute ('length' , 'Ship');
        $this->assertClassHasAttribute ('orientation' , 'Ship');
        $this->assertClassHasAttribute ('start' , 'Ship');
        $this->assertClassHasAttribute ('to' , 'Ship');
        $coord  =  $b->getCoordinates();
        $this->assertInstanceOf('Cell', $coord[0]);
        $this->assertInstanceOf('Cell', $coord[1]);
    }
    /** @test */
    public function Test_Grid_fill()    {
        $data = $this->grid->getDataGrid();
        $this->assertNotNull($data);
        $this->assertContains(0 ,$data['A']);

    }

    public static function data_input_provider() {
        return array(
            array( 'A5'     , true ) ,
            array( 'A0'     , false) ,
            array( 'Z4'     , false) ,
            array( 'DD'     , false) ,
            array( 'DDFFF'  , false) ,
            array( 'Dr5'    , false) ,
            array( 'd5'     , true ) ,
            array( ''       , false) ,
            array( null     , false) ,
            array(  array() , false)
        );
    }

    /**
     * @test
     * @param $coord
     * @param $result
     * @dataProvider data_input_provider
     */

    public function Test_valid_Attack($coord , $result)
    {
        $game = new BattleShipGame();
        $game->debug_console = true;

        $computed = $game->validate($coord);
        $this->assertEquals($result , $computed );
    }
}