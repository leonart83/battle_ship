<?php

/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 5/18/2016
 * Time: 11:26 PM
 */

class BattleShipGame
{

    private static $shot_count = 0;
    private $grid;
    private $ship_factory;


    private $started = false;
    private $miss = null;
    private $output = '';
    private $session_manager;
    private $console_mode   = false;
    private $test_mode      = false;
    private $show     = false;


    private $instruction  = PHP_EOL.'Enter coordinates (row, col), e.g. A5 : ';
    private $error_mgs    = '    *** ERROR ***'.PHP_EOL;
    private $hit_msg      = '    *** HIT ***'.PHP_EOL;
    private $miss_msg     = '    *** MISS ***'.PHP_EOL;
    private $sunk_msg     = '    *** SUNK ***'.PHP_EOL;
    private $end_mgs    = ' Well done! You completed the game in %d shots.'.PHP_EOL;
    private $cols = null;
    private $rows = null;
    private $row_keys = null;
    private $col_keys = null;
    public  $debug_console = true;
    private $ships = array();

    public function __construct()
    {
        // see in which mode we run the game: console , web or test via phpunit
        $this->console_mode =  (php_sapi_name() === 'cli') ? true : false;
        if ($this->console_mode && strpos($_SERVER['argv'][0], 'phpunit') !== false){
            $this->test_mode = true;
        }
        $this->grid = new Grid();
        $this->cols = $this->grid->getColHeaders();
        $this->rows = $this->grid->getRowHeaders();
        $this->col_keys = array_values ($this->grid->getColHeaders());
        $this->row_keys = array_keys($this->grid->getRowHeaders());

        $this->ship_factory     = ShipFactory::getInstance();
        //we rely on session to see if the game started
        if($this->console_mode == false){
            // if($this->console_mode == false ||  ($this->console_mode == true && $this->test_mode = true && $this->debug_console == false  )){
            $this->session_manager  = SessionManager::getInstance();
            if(! isset($_SESSION) || !$this->session_manager->get_started() ) {
                $this->session_manager->start();
            }
            $this->started =   $this->session_manager->get_started();
            if (isset($_POST['show']) && ! empty($_POST['show'])){
                $this->show = 1;
            }else{
                $this->show = 0;
            }
            $this->session_manager->set_show(  (string)  $this->show);
            if ( $this->started === false){
                $this->session_manager->set_shot_count(self::$shot_count); //zero
                $this->start();
            }else{
                //populate data from session
                $this->grid->setDataGrid(   $this->session_manager->get_data_grid()     );
                $this->ships = $this->session_manager->get_ships();
                // we have input from user
                $this->miss = $this->session_manager->get_miss();
                if(isset( $this->miss)){
                    $this->grid->setValue( $this->miss[0],  $this->miss[1], Grid::_WATER);
                    $this->session_manager->set_miss(null);
                }
                if (isset($_POST['coord'])){

                    $valid_input = $this->validate($_POST['coord']);
                    if ($valid_input){
                        $this->updateShots(); //set number of shots
                        //check coordinate of ships and update the grid
                        $hit =     $this->attack($_POST['coord']);
                        if ($hit == false){
                            echo $this->miss_msg;
                        }else{
                            if ($hit == 1){
                                echo $this->hit_msg;
                            }
                            if ($hit == 2){
                                echo $this->sunk_msg;
                            }
                        }
                    }else{
                        echo $this->error_mgs;
                    }
                }
            }
            $this->generate();
            //save the grid in the session
            $this->session_manager->set_data_grid( $this->grid->getDataGrid ()  );
            if ($this->gameOver()){
                echo sprintf($this->end_mgs, $this->session_manager->get_shot_count());
                session_destroy();
            }

        }else{
            //we are in console mode, just one start
            $this->start();
            $this->generate();
            //avoid being stuck in a loop (because in console we wait for user input) while launching unit test with php unit
            $this->loop();

        }
        //session_destroy();
        /*echo $this->session_manager->get_shot_count();
        echo PHP_EOL;
        echo '<pre>';
        print_r($this->ships);
        echo '</pre>';*/

    }
    public function start(){
        $this->ship_factory->createShips();
        $this->ship_factory->placeShips($this->grid);
        $this->ships =  $this->ship_factory->getShips();
        if (!$this->console_mode) $this->session_manager->set_ships($this->ships);
    }
    private function loop(){
        parseCLI:{
            $handle = fopen ("php://stdin","r");
            $user_input = trim(fgets($handle));
        }
        if($user_input == 'exit') {
            echo "EXIT GAME.\n";
            fclose($handle);
            exit;
        }elseif($user_input == 'show') {
            $this->show = true;
            $this->generate();
            goto parseCLI;
        }elseif($user_input == 'hide') {
            $this->show = false;
            $this->generate();
            goto parseCLI;
        }else{
            $valid_input = $this->validate($user_input);
            if ($valid_input){
                $this->updateShots();
                if(isset($this->miss)){

                    $this->grid->setValue( $this->miss[0], $this->miss[1], Grid::_WATER);
                    $this->miss = null;
                }
                $hit = $this->attack($user_input);
                if ($hit == false){
                    echo $this->miss_msg;
                }else{
                    if ($hit == 1){
                        echo $this->hit_msg;
                    }
                    if ($hit == 2){
                        echo $this->sunk_msg;
                    }
                }
            }else{
                echo $this->error_mgs;
            }
            $this->generate();
            if ($this->gameOver()){
                echo PHP_EOL.sprintf($this->end_mgs,self::$shot_count);
                fclose($handle);
                exit;
            }
            goto parseCLI;
        }
    }


    public function generate()
    {

        $this->output = $this->grid->output($this->console_mode ,$this->show);
        echo $this->output;
        echo $this->html_form();
        //echo PHP_EOL.' '.self::$shot_count;
        //echo '<pre>',print_r($this->ship_factory->getShips(),1),'</pre>';

    }

    public function attack($input){
        $arr_input = $this->split_input($input);


        $x = array_search(  $arr_input[1], $this->col_keys);
        $input = $arr_input[0].$arr_input[1];
        foreach($this->ships as $ship){
            $sc  = $ship->getAllCoordinates();
            foreach($sc as $c){
                if($c->coord == $input){

                    $ship->setHit($c);
                    $this->grid->setValue( $arr_input[0] , $x  , Grid::_HIT );
                    if ($ship->checkIsSunked()){
                         
                        foreach($sc as $q){
                            $arr_input = $this->split_input($q->coord);
                            $x = array_search(  $arr_input[1], $this->col_keys);
                            $this->grid->setValue( $arr_input[0] , $x  , Grid::_SUNK );
                        }
                        return 2;
                    }else{
                        return 1;
                    }

                }
            }
        }
        $this->grid->setValue($arr_input[0] , $x   , Grid::_MISS );
        if (!$this->console_mode){
            $this->miss = $this->session_manager->set_miss(array($arr_input[0] , $x  ));
        }else{
            $this->miss = array($arr_input[0] , $x  );
        }
        return false;
    }
    public function gameOver(){
        foreach($this->ships as $ship){
            if (!$ship->isSunk()){
                return false;
            }
        }
        return true;
    }

    public function validate($input){
        if(!isset($input))  return false;
        if( is_array($input) || is_object($input)) return false;
        if(!strlen($input) > 3 && strlen($input) < 2 ) return false;
        $arr_input = $this->split_input($input);
        if (!in_array( $arr_input[0] ,$this->grid->getRowHeaders() )) return false;
        if (!in_array( $arr_input[1] ,$this->grid->getColHeaders() )) return false;
        return true;
    }
    private function split_input($input){
        $letter = substr  ($input, 0, 1);
        $number = substr($input, 1);
        $number *=  1;
        return array( strtoupper ( $letter ) , $number);
    }
    /**
     * @param boolean $console_mode
     */
    public function setConsoleMode($console_mode)
    {
        $this->console_mode = $console_mode;
    }
    public function get_is_test_env(){
        return $this->test_mode;
    }
    /**
     * @return mixed
     */
    public function getSessionManager()
    {
        return $this->session_manager;
    }
    public function getSHipFactory(){
        return $this->ship_factory;
    }
    public function html_form(){
        if (!$this->console_mode) {
            $a= (string) $this->session_manager->get_show();
            $show = ($a == '1')?  'true' :'false';
            $ck = ($a == '1')?  'checked' :'';
            $form = '<form name="input" action="index.php" method="post">
            ' . $this->instruction .'
            <input type="input" size="5" name="coord" autocomplete="off" autofocus="">
            <input type="submit">
           
             <input type="checkbox" name="show" value="'.$show.'" '.$ck.'>
                <label for="checkbox_id">Show</label>
        </form>';
            return $form;
        }else{
            return $this->instruction;
        }


    }

    protected function updateShots()
    {
        if (!$this->console_mode) self::$shot_count = $this->session_manager->get_shot_count();
        self::$shot_count++;
        if (!$this->console_mode) $this->session_manager->set_shot_count(self::$shot_count);
    }
}