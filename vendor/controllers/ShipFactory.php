<?php

/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 5/20/2016
 * Time: 3:20 PM
 */
class ShipFactory
{
    private static $instance;

    /**
     * @return mixed
     */
    public static function getNUMBEROFSHIPS()
    {
        return self::$NUMBER_OF_SHIPS;
    }

    /**
     * @param int $NUMBER_OF_SHIPS
     */
    public static function setNUMBEROFSHIPS($NUMBER_OF_SHIPS)
    {
        self::$NUMBER_OF_SHIPS = $NUMBER_OF_SHIPS;
    }
    private $boats = array();

    /**
     * @return ShipFactory instance
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * ShipFactory constructor.
     */
    protected function __construct(){}


    public function createShips()
    {
        $ships = array( new Ship(4) , new Ship(4) , new Ship(5));
        self::$instance->boats = $ships ;
    }
    /**
     * @return array of Ship
     */
    public function getShips()
    {
        return static::$instance->boats;
    }
    public function placeShips(Grid $grid)
    {
        foreach (self::$instance->boats as $boat){
            $boat->addTo($grid);
        }
    }
    
}
