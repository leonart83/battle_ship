<?php

/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 5/22/2016
 * Time: 3:27 PM
 */
class SessionManager
{
    private static $instance;

    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            static::$instance = new self();
        }

        return self::$instance;
    }
    /**
     * ShipFactory constructor.
     */
    protected function __construct(){}

    public function is_session_started () {

        return function_exists ( 'session_status' ) ? ( PHP_SESSION_ACTIVE == session_status () ) : ( ! empty ( session_id () ) );
    }

    public function start(){
        if (isset($_COOKIE[session_name()]) ){
            if ($_COOKIE[session_name()] != '' ){
                session_id($_COOKIE[session_name()]);
            }
        }
        if(! isset($_SESSION)) {
            session_start();
        }
        $_SESSION['started'] = true;
    }

    public function end(){
        session_destroy();
    }
    public function get_started(){
        return isset($_SESSION['grid']);
    }

    public function get_data_grid(){
        return isset( $_SESSION['grid']) ? $_SESSION['grid'] : null ;
    }
    public function set_data_grid($arr){
        $_SESSION['grid'] = $arr;
    }

    public function get_shot_count(){
        return isset($_SESSION['shot_count']) ? $_SESSION['shot_count'] : null ;
    }
    public function set_shot_count($num)
    {
        $_SESSION['shot_count'] = $num;

    }
    public function get_ships()
    {
        return isset($_SESSION['ships']) ? $_SESSION['ships'] : null ;

    }
    public function set_ships($ships)
    {
        $_SESSION['ships'] = $ships;

    }
    public function get_miss()
    {
        return isset($_SESSION['miss']) ? $_SESSION['miss'] : null ;

    }
    public function set_miss($coord)
    {
        $_SESSION['miss'] = $coord;

    }
    public function get_show()
    {
        return isset($_SESSION['show']) ? $_SESSION['show'] : null ;

    }
    public function set_show($show)
    {
        $_SESSION['show'] = boolval($show);

    }
}