<?php

/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 5/19/2016
 * Time: 1:28 AM
 */

class AutoLoader {
    const CLASS_DIR = 'vendor/';
    private $base_path = null;
    private $load_path = null;
    private $dirs = ['controllers','models'];
    public function __construct($filename=null) {
        spl_autoload_extensions('.php');
        spl_autoload_register(array($this, 'loader'));
        if (isset($filename) && $filename !=''){

            //PHP UNIT fix for the filename passed
            if (strpos($filename, 'tests') !== false) {
                $filename = dirname($filename);
                substr($filename, 0, strpos($filename, 'tests'));
            }
            $this->load_path = $filename. DIRECTORY_SEPARATOR;
            
            

        }
    }

    /**
     * @return null|string
     * @throws LoadPathNotFoundException
     */
    public function get_base_path(){
        if (!isset( $this->load_path)){
            $this->load_path = $this->set_root_path();
        }
        $this->base_path = $this->load_path . self::CLASS_DIR;
        return $this->base_path;
    }
    public function set_root_path(){
        if (isset($_SERVER['argv']) && $_SERVER['argv'] != '' && $_SERVER['argc'] > 0 ){
            //CLI
            // now we need to find which argument contains the base path of the script, cause the magic method __file__ are not reliable on different context.
            //lets loop in the array of arguments.
            foreach ($_SERVER['argv'] as $args){
                if (strpos($args, '.php') !== false) {
                    $this->load_path = dirname(realpath($args));
                    $this->load_path .= DIRECTORY_SEPARATOR;
                    continue;
                }
            }

        }elseif (getcwd() != false){
            //script not launched from command line.
            //get current working directory
            $this->load_path = getcwd();
            $this->load_path.= DIRECTORY_SEPARATOR;
        }else{
            $cdr = $_SERVER['CONTEXT_DOCUMENT_ROOT'];
            $dr  = $_SERVER['DOCUMENT_ROOT'];
            if(isset($cdr ) && $cdr != ''){
                $this->load_path = ($cdr == $dr) ?  $cdr :  $dr;
            }elseif(isset($dr) && $dr != ''){
                $this->load_path = $dr;
            }else {
                throw new LoadPathNotFoundException('cannot evaluate base script path');
            }
            $this->load_path .= $_SERVER['PHP_SELF'];
            $this->load_path = str_replace(basename($_SERVER["SCRIPT_FILENAME"]),'', $this->load_path);
        }
        return $this->load_path;
    }
    /**
     * @param
     * @return bool|mixed
     */
    private function loader($className) {
        foreach ($this->dirs as $dir){
            $filename =  $this->get_base_path() . $dir. "/" . $className . ".php";
            if (file_exists($filename)) {
                if (is_readable($filename)) {

                    return require_once $filename;

                }
            }
        }
        return false;
    }
}
