<?php

/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 5/22/2016
 * Time: 7:44 PM
 */
class Coordinates
{

    protected $coord;
    protected $hit = false;
    public function __construct($value = null ){
        $this->coord = $value;
    }

    /**
     * @param $name
     * @return int
     */
    public function __get($name)
    {
        if($name == 'coord') return $this->coord;
        if($name == 'hit') return $this->hit;

    }

    /**
     * @param $name
     * @param $value int
     */
    public function __set($name, $value)
    {
        if($name == 'coord') $this->coord = $value;
        if($name == 'hit') $this->hit = $value;

    }

}