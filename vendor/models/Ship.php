<?php

/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 5/18/2016
 * Time: 11:26 PM
 */
// PHP 5.3 and later:

class Ship
{
    private $length = 0;
    private $added_level= 0 ;
    private $sunk = false;

   
    private $start = null;
    private $to = null;
    private $orientation;
    private $placed = false;
    private $added = false;
    private $coordinates = array();

    /**
     * @return array
     */
    public function getAllCoordinates()
    {
        return $this->coordinates;
    }
    /**
     * @return integer
     */
    public function getOrientation()
    {
        return $this->orientation;
    }

    const HORIZONTAL = 0;
    const VERTICAL = 1;

    public function __construct($len)
    {
        $this->length = $len;
        $this->coordinates = array();
        $this->init();
    }
    public function reset(){
        $this->added_level = 0 ;
        $this->sunk = false;
        $this->placed = false;
        $this->added = false;
    }
    public function setHit($x){
        foreach($this->coordinates as $c){
            if ($c->coord == $x->coord ){
                $c->hit = true;
                return;
            }
        }
    }
    public function checkIsSunked(){
        foreach($this->coordinates as $c){
            if ($c->hit == false){
                return false;
            }
        }
        $this->sunk = true;
        return $this->sunk;
    }
    /**
     * @return boolean
     */
    public function isSunk()
    {
        return $this->sunk;
    }
    
    public function setAddedLevel()
    {
        $this->added_level = count($this->coordinates);
        if ( $this->added_level ==  $this->length){
            $this->added = true;
        }
    }


    /**
     * @return $this
     */
    public function init(){

        $rows = 9 ; // count($data_grid);
        $cols = 9 ; // count($data_grid[0]);
        $this->reset();
        $this->start = new Cell( ) ;
        $this->to   = new Cell( ) ;
        $this->start->x =  rand(0, $rows);
        $this->start->y =  rand(0, $cols);

        $this->orientation = rand( self::HORIZONTAL , self::VERTICAL);

        switch ( $this->orientation ) {
            case self::HORIZONTAL :
                $this->to->x = (( $this->start->x + $this->length) > $rows) ? $this->start->x - $this->length : $this->start->x + $this->length;
                $this->to->y = $this->start->y;
                break;
            case self::VERTICAL:
                $this->to->x =$this->start->x;
                $this->to->y =  ( ( $this->start->y +  $this->length) > $cols)?  $this->start->y -  $this->length :$this->start->y +  $this->length;
                break;

        }
        return $this;
    }

    /**
     * @return null
     */
    public function getCoordinates()
    {
        return array($this->start, $this->to) ;
    }

    /**
     * @return null
     */
    public function setCoordinates(Cell $start , Cell $to)
    {
        $this->start = $start ;
        $this->to = $to;
        if ($this->start->x == $this->to->x && ($this->start->y != $this->to->y) ) {
            $this->orientation = self::VERTICAL ;
        }else{
            $this->orientation = self::HORIZONTAL ;
        }

    }
    public function addTo( Grid $grid){
        $temp       = array();
        $temp_grid  = $grid->getDataGrid();
        $keys       = array_keys($temp_grid);


        while ( !( $this->placed == true && $this->added == true)   ){
            if  ( $this->orientation == self::HORIZONTAL ) {
                $min = min($this->start->x, $this->to->x);
                $max = max($this->start->x, $this->to->x);
                $row = $temp_grid[$keys[$this->to->y]];
                if ($this->placed == false && $this->added == false) {
                    $temp       = array();
                }
                for ($r = $min; $r < $max; $r++) {
                    //collect values to check if space is present by reading grid data array....
                    if ($this->placed == false && $this->added == false) {
                        $temp[] = $row[$r];
                    }
                    //already checked and there is space to palce the ship ?
                    //write in the grid and id for the cell value, so we know
                    //that there is a bout there.
                    if ($this->placed == true && $this->added == false) {
                        //we start to write in the data, to keep trak
                        //of the final write to set the boolean we increment
                        //internal variable added_level, which will much the length and set added to true
                        $grid->setValue($keys[$this->to->y], $r, Grid::_SHIP);
                        $coord = $r+1;
                        $coord = $keys[$this->to->y] . $coord ;
                        $this->coordinates[] = new Coordinates ($coord);
                        $this->setAddedLevel();
                    }
                }
            }else{
                $min = min($this->start->y ,$this->to->y);
                $max = max($this->start->y ,$this->to->y);
                $y = 0;
                if ($this->placed == false && $this->added == false) {
                    $temp       = array();

                }
                foreach ($temp_grid as $key => $row){
                    if ($y >= $min && $y <= $max){
                        //collect values to check if space is present by reading grid data array....
                        if ($this->placed == false && $this->added  == false){
                            $temp[] = $row[$this->start->x];
                        }
                        if ($this->placed == true && $this->added  == false){
                            //$row[$y] = Grid::_SHIP;
                            $grid->setValue($key, $this->start->x , Grid::_SHIP );
                            //keep in account offset by one on rows caused by the fact array are based zero
                            $coord = $this->start->x + 1;
                            $coord = $key . $coord ;
                            $this->coordinates[] = new Coordinates ($coord);
                            $this->setAddedLevel();
                        }
                    }
                    $y ++;
                }

            }
            if (! $this->added ) {
                $this->placed = self::validate_space($temp);
            }
            //non placed ? grid is occupied , re-init the ship with its random values.
            if (! $this->placed ){
                $this->init($this->length);
            }

        }
        return true;
    }

    public static function validate_space($temp){
        $v = array_unique($temp);
        $c = count($v);
        if ($c === 1 && $v[0] == Grid::_WATER){
            return true;
        }else{
            return false;
        }
    }
}
