<?php

class Grid
{

    private $data_grid;
    private $output;
    private $show = false;
    const ROWS  = 10;
    const COlS  = 10;

    const _WATER = 0; const WATER   = '.';
    const _MISS  = 1; const MISS    = '-';
    const _SHIP  = 2; const SHIP    = 'X';
    const _HIT   = 3; const HIT     = 'X';
    const _SUNK  = 4; const SUNK    = 'X';

    public function __construct()
    {
        $this->col_headers = range(1,  self::ROWS);
        $this->row_headers = range('A', 'J');
        $this->init();
    }

    /**
     * @return array
     */
    public function getColHeaders()         {
        return $this->col_headers;
    }

    /**
     * @return array
     */
    public function getRowHeaders()        {
        return $this->row_headers;
    }
    private function init(){
        for ($i = 0 ; $i < count($this->row_headers); $i++){
            $this->data_grid[$this->row_headers[$i]] =  array_fill(0 , self::COlS , self::_WATER  );
        }
        return $this->data_grid;
    }
    public  function output($console_mode=false , $show=false){
        //html pre tag for output , not present in console mode
        $this->output = ($console_mode) ? '' :  '<pre>';
        $this->output .= '  ';
        $this->output .= implode(' ', $this->col_headers);
        $this->output .= PHP_EOL;
        //loop on rows first, which have the letters on the left
        for ($r = 0; $r < self::ROWS; $r++) {
            $row_key = $this->row_headers[$r];
            $this->output .=   $this->row_headers[$r] ;
            //loop on columns
            for ($col = 0; $col < self::COlS; $col++) {
                $value =   $this->getValue( $row_key ,$col) ;

                    $value =  $this->output_cell_value($value , $show );


                $this->output.= ' ' . $value ;
            }
            $this->output .= ($console_mode) ? PHP_EOL : PHP_EOL;
        }
        $this->output .= ($console_mode) ? '' :  '</pre>';
        return $this->output;
    }

    public function get_rows_count(){
        return self::ROWS;
    }
    public function get_cols_count(){
        return self::COlS;
    }


    /**
     * @return mixed
     */
    public function getDataGrid()
    {
        return $this->data_grid;
    }

    /**
     * @param mixed $data_grid
     */
    public function setDataGrid($data_grid)
    {
        $this->data_grid = $data_grid;
    }

    /**
     * @param $key
     * @param $col
     * @param $val
     */
    public function setValue($key , $col , $val)
    {
        $this->data_grid[$key][$col] =   $val ;
    }
    public function getValue($key , $col )
    {
        return $this->data_grid[$key][$col] ;
    }
    private function output_cell_value($n , $show){
        $value = null;
        switch ( $n ) {
            case self::_WATER : //0
                $value = self::WATER;
                break;
            case self::_MISS : //1
                $value = self::MISS;
                break;
            case self::_SHIP : //2
                $value =  (boolval($show)) ?  self::SHIP :  self::WATER;
                break;
            case self::_HIT: //3  --> X
                $value = self::HIT;
                break;
            case self::_SUNK: //4 --> X
                $value = self::SUNK;
                break;
            default:
                $value = '';
        }
        return $value;
    }
}
